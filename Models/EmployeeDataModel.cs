﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employees.Models
{
    public class EmployeeDataModel
    {
        public string empid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string mail { get; set; }
    }
}
