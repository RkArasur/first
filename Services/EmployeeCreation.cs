﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employees.Repos;
using Employees.Models;
using Newtonsoft.Json;
namespace Employees.Services
{
    public class EmployeeCreation
    {
        //EmployeeDataModel e = new EmployeeDataModel() { empid = eid, fname = fn, lname = ln, mail = email };
       // System.IO.File.WriteAllText(@"C:\Users\RavikiranArasur\source\repos\Employees\References\EmployeeData.json", JsonConvert.SerializeObject(e));
        public void createEmployee(string eid,string fn,string ln,string email)
        {
            var filePath = @"C:\Users\RavikiranArasur\source\repos\Employees\References\EmployeeData.json";
            var jsonData = System.IO.File.ReadAllText(filePath);
            var employeeList = JsonConvert.DeserializeObject<List<EmployeeDataModel>>(jsonData) ?? new List<EmployeeDataModel>();
            employeeList.Add(new EmployeeDataModel() {
                empid = eid,
                fname = fn,
                lname = ln,
                mail = email
            });
            jsonData = JsonConvert.SerializeObject(employeeList);
            System.IO.File.WriteAllText(filePath, jsonData);
        }
        public EmployeeDataModel[] getEmployees()
        {
            var filePath = @"C:\Users\RavikiranArasur\source\repos\Employees\References\EmployeeData.json";
            var jsonData = System.IO.File.ReadAllText(filePath);
            var employeeList = JsonConvert.DeserializeObject<List<EmployeeDataModel>>(jsonData) ?? new List<EmployeeDataModel>();
            return employeeList.ToArray();
        }
        public void editEmployee(string e,string fn,string ln,string mail)
        {
            var filePath = @"C:\Users\RavikiranArasur\source\repos\Employees\References\EmployeeData.json";
            var jsonData = System.IO.File.ReadAllText(filePath);
            var employeeList = JsonConvert.DeserializeObject<List<EmployeeDataModel>>(jsonData) ?? new List<EmployeeDataModel>();
            foreach (EmployeeDataModel emp in employeeList)
            {
                if(emp.empid == e)
                {
                    emp.empid = e;
                    emp.fname = fn;
                    emp.lname = ln;
                    emp.mail = mail;
                }
            }
            jsonData = JsonConvert.SerializeObject(employeeList);
            System.IO.File.WriteAllText(filePath, jsonData);
        }
    }
}
