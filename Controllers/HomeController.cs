﻿using Employees.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;

namespace Employees.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        { 
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(LoginModel auth)
        {
            Dictionary<string, string> users = new Dictionary<string, string>();
            users.Add("user1","1");
            users.Add("user2", "Royal@123");
            users.Add("user3", "Student@420");
            users.Add("user4", "learner#4");
            string user = auth.username;
            string pwd = auth.password;
            if (users.ContainsKey(user))
            {
                if(users[user] == pwd)
                {
                    HttpContext.Session.SetString("usersession", user);
                    return RedirectToAction("Index", "Login");
                }
                else
                {
                    TempData["message"] = "Invalid Password";
                }
            }
            else
            {
                TempData["message"] = "User does not exist";
            }
            return RedirectToAction("Index");
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}