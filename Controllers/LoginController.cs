﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employees.Repos;
using Employees.Services;
using Employees.Models;
using Newtonsoft.Json;
namespace Employees.Controllers
{
    public class LoginController : Controller
    {
        static EmployeeCreation creator = new EmployeeCreation();
        List<EmployeeViewModel> emps = new List<EmployeeViewModel>(); 
        public IActionResult Index()
        {
            var user = HttpContext.Session.GetString("usersession");
            if(user != null) { 
                EmployeeDataModel[] employees = creator.getEmployees();
                for (int i = 0; i < employees.Length; i++)
                {
                    string eid = employees[i].empid;
                    string fn = employees[i].fname;
                    string ln = employees[i].lname;
                    string mid = employees[i].mail;
                    emps.Add(new EmployeeViewModel
                    {
                        empid = eid,
                        fname = fn,
                        lname = ln,
                        mail = mid
                    });
                }
                ViewBag.Emps = emps.ToArray();
                ViewBag.use = user;
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public IActionResult createEmployee(EmployeeViewModel e)
        {
            creator.createEmployee(e.empid, e.fname, e.lname, e.mail);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult editDetails(string empid, string fname, string lname, string mail)
        {
            creator.editEmployee(empid, fname, lname, mail);
            return RedirectToAction("Index");
        }
        public IActionResult Search()
        {
            List<EmployeeViewModel> b = new List<EmployeeViewModel>();
            var user = HttpContext.Session.GetString("usersession");
            if (user != null)
            {
                if (TempData["result"] != null)
                {
                    List<EmployeeDataModel> em = new List<EmployeeDataModel>();
                    em = JsonConvert.DeserializeObject<List<EmployeeDataModel>>((string)JsonConvert.DeserializeObject((JsonConvert.SerializeObject(TempData["result"]))));
                    var a = em.ToArray();
                    for (int i = 0; i < a.Length; i++)
                    {
                        string eid = a[i].empid;
                        string fn = a[i].fname;
                        string ln = a[i].lname;
                        string mid = a[i].mail;
                        b.Add(new EmployeeViewModel
                        {
                            empid = eid,
                            fname = fn,
                            lname = ln,
                            mail = mid
                        });
                    }
                }
                ViewBag.Emps = b.ToArray();
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        [HttpPost]
        public IActionResult SearchEmployee(string name)
        {
            EmployeeDataModel[] emp = creator.getEmployees();
            List<EmployeeDataModel> em = emp.Where(e => e.fname.Contains(name) || e.lname.Contains(name)).ToList();
            TempData["result"] = JsonConvert.SerializeObject(em);
            return RedirectToAction("Search");
        }
    }
}
