﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employees.Repos
{
    public class Employee
    {
        public string firstname;
        public string lastname;
        public string mail;
        public string EmpID;
        
        public Employee()
        {

        }
        public Employee(string eid, string fname, string lname, string mail)
        {
            this.EmpID = eid;
            this.firstname = fname;
            this.lastname = lname;
            this.mail = mail;
        }
        public string[] getInfo()
        {
            string[] e = new string[] { this.EmpID, this.firstname, this.lastname, this.mail };
            return e;
        }
    }
}
